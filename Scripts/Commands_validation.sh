#Shell/Bash script

#Validation of DM XGB model predicting CE genes

for i in {1..18461}; do head -$i DM_XGB.txt > temp.txt; fgrep -f temp.txt Genes_lethal_alleles_GExplore.txt | wc -l >> Results_DM_head_XGB.txt;done

for i in {1..18461}; do head -$i DM_XGB.txt > temp.txt; fgrep -f temp.txt Genes_lethal_alleles_GExplore.txt | wc -l >> Results_DM_tail_XGB.txt;done

#Validation of CE XGB model predicting DM genes

for i in {1..11580}; do head -$i CE_XGB.txt > temp.txt; fgrep -f temp.txt GenomeRNAi_v17_Drosophila_melanogaster.txt | egrep -h "Lethal|lethal|decreased cell number|decreased viability|Decreased cell number|Decreased viability|decreased cell viability|low cell number" | awk '{print $3}' | tr -d "," | sort | uniq | grep FBgn | wc -l >> Results_CE_head_XGB.txt; done

for i in {1..11580}; do tail -$i CE_XGB.txt > temp.txt; fgrep -f temp.txt GenomeRNAi_v17_Drosophila_melanogaster.txt | egrep -h "Lethal|lethal|decreased cell number|decreased viability|Decreased cell number|Decreased viability|decreased cell viability|low cell number" | awk '{print $3}' | tr -d "," | sort | uniq | grep FBgn | wc -l >> Results_CE_tail_XGB.txt; done

#Use Results files for Figure 3 plot
