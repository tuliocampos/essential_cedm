# Cross-predicting essential genes between two model eukaryotes using machine learning #


* To download this repository:

```
git clone https://www.bitbucket.org/tuliocampos/essential_CEDM.git
```
- Requirements: R 3.6+ (systematic feature selection and machine-learning); R 4.0+ (chromoMap plot)


## How to use? ##

This repository allows you to run the systematic machine learning (ML) approaches for essential gene predictions without manual intervention. All the required scripts and data are readily available.  

 **1.** Install the R libraries listed in the file "./Scripts/LIBRARIES".

 **2.** Because the lists of genes provisionally annotated for essentiality ("./Genes\_and\_Sequences") and their respective features ("./Features/Feature\_collection\_CEDM.txt") are readily available (obtained from Campos et al. 2020a,b), the R scripts ("./Scripts/\*.R") encompass all the steps required for the ML analyses: load the feature data, label the genes, and run the systematic subsampling/feature selection/ML approaches. 

To run The systematic approach within/between species using the CE or DM (including bootstrap cross-validations), use the following commands (nohup or screen are recommended to run in the background):

```

#Enter directory
cd Scripts

#Run the machine learning (ML) using CE data: within species approach, followed by cross-species predictions
Rscript PRED_CE_script.R

#Run bootstrap ML using CE data (within species)
Rscript 1000bootstraps_CE_script.R

#To run the systematic approach within and cross-species using the DM genes/features

Rscript PRED_DM_script.R

#Bootstrap ML with DM data (within species)
Rscript 1000bootstraps_DM_script.R

```

These commands will load the required genes and features for each of the data sets, and run the sub-sampling and feature selection followed by ML approaches.

The results will be written into the current directory.

You can also run the scripts using Rstudio! 

Modify the scripts if you want to use more or less than 32 CPU cores.

 **3.** If you want to explore the feature data, load the collection into an R data.frame (columns represent features, rows represent genes):
 
```
x=read.delim("./Features/Feature_collection_CEDM.txt",sep="\t",header=T,row.names=1)

#Shows the first feature names
head(colnames(x))

#Shows the first gene names
head(rownames(x))
``` 

Filters can be applied to select features and/or genes by their names. For example, selecting only the 34 highly-predictive features in both CE and DM identified in the study:

```
library(dplyr)
best_pred=read.delim("./Features/features_COMMON.txt",header=F)
x=x %>% dplyr::select(one_of(as.character(best_pred$V1)))

#the CE_features.txt and DM_features.txt files correspond to the best features identified here for each species, respectively.

```

To select "essential" or "non-essential" gene features, load the "./Genes\_and\_Sequences/\*PRED.txt" files and use as filters (use the "\*NR.txt files for the non-redundant sequences").

For example, to select features of the initial/provisional set of essential genes in the CE set (from Campos et al.,2020a):

```
essential=read.delim("./Genes_and_Sequences/Essential_CE_PRED.txt",header=F)
x=x[which(rownames(x) %in% as.character(essential$V1)),]
```

To convert the data.frame into a matrix format (if necessary, for ML inputs for example):

```
x_names=rownames(x)
x=sapply(x,as.numeric)
rownames(x)=x_names
```

 **4.** To apply the ML approach for other species (new data):

 - Create a collection of features from genes and save it in the same format as the Feature\_collection.txt file, with rows representing the genes, and columns representing the features. You can focus on the 34 highly predictive features identified here "./Features/features\_COMMON.txt", for example. 
 - Create two files containing the lists of essential or non-essential gene names, one name per line (see the \*.list files)
 - Modify the inputs in the PRED\_script.R, pointing to the new files
 - Run the R script as described in **2.**
 - For predictions between species, you can use the scripts train models with CE or DM genes/features, and then predict essentiality of genes in the new species, provided the same set of features is available. You can focus on the 34 highly predictive features identified here "./Features/features\_COMMON.txt", for example.

---------------------------------------------------------------------

## Files and Directories ##

* Software and library versions:  sessionInfo\*.txt
* Licensing: LICENCE

Genes and Sequences data:

* Gene lists per essentiality annotation: "\*PRED.txt" files, and corresponding genomic locations "\*.gff"

Features:

* The file containing the features extracted for each gene (features with low variance were removed): "Feature\_collection\_CEDM.txt"
* Details about the collection: see Campos et al. 2020a,b 
* The final set of 34 highly predictive features of gene essentiality: features\_COMMON.txt

Scripts:

* Scripts used for the systematic feature selection and machine-learning approaches: see the ".R" files
* Additional R and BASH scripts for feature extraction and plots: for reference only; refer to the Materials and Methods section in the manuscript

Data for plots (Data\_plots):

* Coordinate files for the assessment of genomic locations: "\*.coord" files
* Ordered essentiality scores for plotting: Essentiality\_scores.txt

## Licence ##

MIT

## Cite ##

If you use these scripts or data, please cite: 

**Cross-Predicting Essential Genes between Two Model Eukaryotic Species Using Machine Learning**

Tulio L. Campos, Pasi K. Korhonen, Neil D. Young

**International Journal of Molecular Sciences**, 22(10), 5056; https://doi.org/10.3390/ijms22105056   
