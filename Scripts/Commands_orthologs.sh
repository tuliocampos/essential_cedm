#Counts the number of protein identifiers in the ortholog groups of each input gene
#Download the oma-ensembl.txt and oma-groups files from the OMA database: https://omabrowser.org/oma/current/
for i in `cat ../Genes_and_sequences/ALL_genes.txt`; do grep $i oma-ensembl.txt | awk '{print $1}' | xargs -I{} grep {} oma-groups.txt | xargs -I{} grep {} oma-groups.txt | awk -F "\t" 'NF {print NF-1}' | awk 'END { if (NR==0) print "0"; else print $1}' >> ALL_genes_Counts_Orthologs.txt ;done
